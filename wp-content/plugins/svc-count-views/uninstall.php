<?php

if(!defined('WP_UNINSTALL_PLUGIN')) exit;

include dirname(__FILE__).'/svc-check.php';

/*
 * Удаление плагина
 */
// Если поле 'svc_views' существует
if(svc_check_field('svc_views')){

	global $wpdb;
	// Удаление поля 'svc_views'
	$query = "ALTER TABLE $wpdb->posts DROP svc_views";
	$wpdb->query($query);
}