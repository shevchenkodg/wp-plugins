<?php
/*
Plugin Name: Количество просмотров статей
Description: Плагин считает и выводит количество просмотров статей
Plugin URI: -
Author: -
Author URI: -
*/

include dirname(__FILE__).'/svc-check.php';

/*
 * Активация плагина
 */
register_activation_hook(__FILE__, 'svc_create_field');
function svc_create_field() {
	global $wpdb;

	// Если поле svc_views не существует
	if(!svc_check_field('svc_views')){
		// Добавляем в таблицу wp_posts поле для сохранения количетсва просмотренных записей
		$query = "ALTER TABLE $wpdb->posts ADD svc_views INT NOT NULL DEFAULT '0'";
		$wpdb->query($query);
	}

}

/*
 * Фильтр отображения количества просмотров на странице
 */
add_filter('the_content', 'svc_post_views');
function svc_post_views($content) {
	// Если это страница - возвращаем ее контент
	if(is_page()) return $content;

	global $post;
	$views = $post->svc_views;

	// Если мы находимся в просмотре отдельной статьи
	if(is_single()) $views += 1;
	return $content.'<b>Количество просмотров: </b>'.$views;
}
add_action('wp_head', 'svc_add_view');
function svc_add_view() {
	// Если мы не находимся в отдельной статье
	if(!is_single()) return;

	global $post, $wpdb;

	$svc_id = $post->ID;
	$views = $post->svc_views + 1;
	$wpdb->update(
		$wpdb->posts,
		array('svc_views' => $views),
		array('ID' => $svc_id)
	);
}